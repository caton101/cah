# Enums are not part of the standard library and must be imported.
from enum import Enum

# The __init__ function is required by Python and always called to initialize
# modules. Nothing in this module needs to be initialized so just call pass.
# BEHAVIOR:
#     does nothing
def __init__():
    # tell Python not to do anything
    pass

# This is an enum class that defines all of the possible forground colors. The
# names of the enums represent the color produced and the value stores the
# escape code needed to generate a particular color.
class foregroundColor(Enum):
    WHITE = "\033[39m"
    BLACK = "\033[30m"
    LIGHT_GREY = "\033[37m"
    DARK_GREY = "\033[90m"
    RED = "\033[91m"
    LIGHT_RED = "\033[31m"
    ORANGE = "\033[33m"
    YELLOW = "\033[93m"
    GREEN = "\033[32m"
    LIGHT_GREEN = "\033[92m"
    LIGHT_TEAL = "\033[96m"
    TEAL = "\033[36m"
    LIGHT_BLUE = "\033[94m"
    BLUE = "\033[34m"
    PURPLE = "\033[35m"
    MAGENTA = "\033[95m"

# This is an enum class that defines all of the possible background colors. The
# names of the enums represent the color produced and the value stores the
# escape code needed to generate a particular color.
class backgroundColor(Enum):
    BLACK = "\033[49m"
    LIGHT_GREY = "\033[47m"
    DARK_GREY = "\033[100m"
    RED = "\033[101m"
    LIGHT_RED = "\033[41m"
    ORANGE = "\033[43m"
    YELLOW = "\033[103m"
    GREEN = "\033[42m"
    LIGHT_GREEN = "\033[102m"
    LIGHT_TEAL = "\033[106m"
    TEAL = "\033[46m"
    LIGHT_BLUE = "\033[104m"
    BLUE = "\033[44m"
    PURPLE = "\033[45m"
    MAGENTA = "\033[105m"

# The writeline function is essentially a call to Python's print statement with
# extra data added to it. The function first adds the values of the specified
# foreground and background enums together. If no foreground or background
# colors are provided, it uses a white foreground and a black background. It
# then adds the text argument behind the formatting codes. After adding the
# text, it adds the formatting codes for a white foreground and a black
# background to the string and writes everything to the terminal.
# BEHAVIOR:
#     prints text to the terminal with the specified foreground and
#     background colors
# FORMULA:
#     foreground + background + text + white foreground + black background
def writeline(text, fgColor=foregroundColor.WHITE, bgColor=backgroundColor.BLACK):
    print(fgColor.value + bgColor.value + text + foregroundColor.WHITE.value
        + backgroundColor.BLACK.value)

# The write function is essentially a call to Python's print statement with
# extra data added to it. The function first adds the values of the specified
# foreground and background enums together. If no foreground or background
# colors are provided, it uses a white foreground and a black background. It
# then adds the text argument behind the formatting codes. After adding the
# text, it adds the formatting codes for a white foreground and a black
# background to the string and writes everything to the terminal. The print
# statement is also called with `end=''` so the print statement doesn't add
# a new line after printing.
# BEHAVIOR:
#     prints text to the terminal with the specified foreground and
#     background colors
# FORMULA:
#     foreground + background + text + white foreground + black background
def write(text, fgColor=foregroundColor.WHITE, bgColor=backgroundColor.BLACK):
    print(fgColor.value + bgColor.value + text + foregroundColor.WHITE.value
        + backgroundColor.BLACK.value,end='')
