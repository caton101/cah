# needed for database io
import sqlite3
# needed to make ID tags
import time

# make class for database io
class SQLEngine(object):
    # make object for database io
    def __init__(self, datapath):
        # save path to data and table name
        self.datapath = datapath
        # open connection
        self.establishConnection()
        
    # initialize database
    def establishConnection(self):
        # open connection to database file
        self.connection = sqlite3.connect(self.datapath)
        # get a cursor
        self.cursor = self.connection.cursor()
        # create tables if needed
        self.cursor.execute('create table if not exists BLACK (pack text, sentence text)')
        self.cursor.execute('create table if not exists WHITE (pack text, sentence text)')
    
    # save all changes and close the database
    def closeConnection(self):
        # close the cursor
        self.cursor.close()
        # save any uncommitted changes
        self.connection.commit()
        # close the connection
        self.connection.close()
        
    # add a new black card to the database
    def addBlackCard(self, pack, sentence):
        # check if card already exists
        self.cursor.execute("SELECT * FROM BLACK WHERE pack=? AND sentence=?", (pack, sentence))
        if len(self.cursor.fetchall()) == 0:
            # add a new card to the table
            self.cursor.execute("INSERT INTO BLACK VALUES (?,?)", (pack, sentence))
            # save changes
            self.connection.commit()
            
    # add a new black card to the database
    def addWhiteCard(self, pack, sentence):
        # check if card already exists
        self.cursor.execute("SELECT * FROM WHITE WHERE pack=? AND sentence=?", (pack, sentence))
        if len(self.cursor.fetchall()) == 0:
            # add a new card to the table
            self.cursor.execute("INSERT INTO WHITE VALUES (?,?)", (pack, sentence))
            # save changes
            self.connection.commit()
    
    # get a random black card from the database
    def getSingleBlackCard(self):
        # get a random black card
        self.cursor.execute("SELECT sentence FROM BLACK ORDER BY RANDOM() LIMIT 1")
        # return card
        return self.cursor.fetchone()[0]
    
    # get a random white card from the database
    def getSingleWhiteCard(self):
        # get a random white card
        self.cursor.execute("SELECT sentence FROM WHITE ORDER BY RANDOM() LIMIT 1")
        # return card
        return self.cursor.fetchone()[0]
    
    # get a list of all black cards from the database
    def getAllBlackCards(self):
        # get all cards from the black table
        self.cursor.execute("SELECT sentence FROM BLACK")
        # make container for sentences
        sentences = []
        # put all sentences in a list
        for sentence in self.cursor.fetchall():
            sentences.append(sentence)
        # return matches
        return sentences
    
    # get a list of all white cards from the database
    def getAllWhiteCards(self):
        # get all cards from the white table
        self.cursor.execute("SELECT sentence FROM WHITE")
        # make container for sentences
        sentences = []
        # put all sentences in a list
        for sentence in self.cursor.fetchall():
            sentences.append(sentence)
        # return matches
        return sentences

    # delete a black card from the database
    def deleteBlackCard(self, pack, sentence):
        # request card deletion
        self.cursor.execute("DELETE FROM BLACK WHERE pack=? AND sentence=?", (pack,sentence))
        # save changes
        self.connection.commit()

    # delete a white card from the database
    def deleteWhiteCard(self, pack, sentence):
        # request card deletion
        self.cursor.execute("DELETE FROM WHITE WHERE pack=? AND sentence=?", (pack,sentence))
        # save changes
        self.connection.commit()