import sys
import os
from sqlEngine import SQLEngine

def argCheck():
    # SANITY CHECK: correct number of arguments
    if len(sys.argv) != 3:
        print("ERROR: Invalid number of arguments!")
        return False
    # get two file paths
    readPath = sys.argv[1]
    writePath = sys.argv[2]
    # SANITY CHECK: first argument is the file to read
    if not os.path.isfile(readPath):
        print("ERROR: First argument must be a file!")
        return False
    print("Reading path is valid.")
    # SANITY CHECK: second argument is the file to write
    if not os.path.isfile(writePath):
        print("ERROR: Second argument must be a file!")
        return False
    print("Writing path is correct")
    # all checks have passed at this point
    return True

def getPackName():
    name = ""
    while True:
        name = input("Enter pack name: ")
        if len(name) == 0:
            print("ERROR: Pack name can not be empty!")
        elif name[len(name) - 1] == " ":
            print("ERROR: Pack name can not end with whitespace.")
        elif name[len(name) - 1] == "\t":
            print("ERROR: Pack name can not end with whitespace.")
        else:
            print("Pack name is valid.")
            break
    return name

def getCardColor():
    color = ""
    while True:
        color = input("What color are the cards (WHITE/BLACK)? ")
        color = color.lower()
        if color == "black" or color == "white":
            print("Color is valid.")
            break
        else:
            print("ERROR: Color is invalid.")
    return color

def convertSentence(sentence, mode):
    # copy string
    modified = sentence
    # convert over
    if modified[len(modified)-1] == "\n":
        modified = modified[:len(modified)-1]
    while modified[len(modified)-1] == " ":
        modified = modified[:len(modified)-1]
    if mode == "black":
        if not "_" in modified:
            if modified[len(modified)-1] == "?":
                modified += " _."
        # count the blanks
        if modified.count("_") == 0:
            print("Card needs a human to convert it.")
            print("Original card: %s"%modified)
            modified = input("Modified card: ")
    if mode == "white":
        if modified[len(modified)-1] == ".":
            modified = modified[:len(modified)-1]
    # return modified sentence
    return modified

def processFile(packName, cardColor, readPath, writePath):
    # make container for sentences
    sentences = []
    # fetch sentences
    with open(readPath) as r:
        sentences = r.readlines()
    # open SQL Engine
    sql = SQLEngine(writePath)
    # convert lines
    for sentence in sentences:
        if sentence == "" or sentence == "\n":
            print("Line is empty. Skipping.")
        else:
            converted = convertSentence(sentence, cardColor)
            if cardColor == "white":
                print("[white][%s] %s"%(packName, converted))
                sql.addWhiteCard(packName, converted)
            elif cardColor == "black":
                print("[black][%s] %s"%(packName, converted))
                sql.addBlackCard(packName, converted)
            else:
                print("ERROR: Color is invalid!")
    # close SQL Engine
    sql.closeConnection()

def main():
    if argCheck():
        readPath = sys.argv[1]
        writePath = sys.argv[2]
        packName = getPackName()
        cardColor = getCardColor()
        processFile(packName, cardColor, readPath, writePath)

main()